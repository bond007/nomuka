---
layout: post
title: Nomuka Tsunami Relief
author: Lupe Hume
categories: [Misc]
tags: [tsunami]
---

On January 15 2022 an underwater volcanic eruption occurred that was
heard throughout the Pacific, as far away as Alaska[^fn-alaska]. This
was followed by a tsunami affecting the entire Pacific region -- these
waves reached as far away as Peru in South America.

The devastation changed the island Kingdom of Tonga in so many ways. At
this point in time it was the little yet vitally significant things that
counted: from one's faith in God the Almighty, one's relationship with
their loved ones (love and family), to the overall outlook on life
itself (hope).  Communication was down for days, and not being able to
know whether our loved ones were safe was beyond heart-breaking.
Desperately flicking through the Internet news, I realised Tongans all
over the world were united in prayers for our beloved island kingdom. A
few days passed and New Zealand news came with some hope; not much, but
enough to put us at ease.

Miraculously there were only three casualties throughout Tonga. In the
main islands of Tongatapu and Eua, infrastructure and villages near the
coast were ruined, and the low lying islands close to the underwater
eruption were heavily impacted. Not to mention, ash from the eruption
covered most of Tonga, affecting the air quality and water.

## Support from the Melbourne community for Nomuka

To help the people following this disaster, local schools, businesses
and families gathered donations to be sent in a container to Nomuka. I
would like to take this opportunity to personally thank these respective
donors for their support.

* [Conan Cargo](https://conancargo.com.au/) (51 Access Way, Carrum
Downs).  Conan Cargo is one of the most reliable and dedicated companies
who ship to anywhere in the Pacific. The also import island food from
Tonga: cassava, &#699;ufi, taro and kape, as well as kava for those who love
the social, relaxing kava drinking in the weekend after a hard week of
work.  Conan Cargo were on-board with [Island Rose
Dreams](http://www.islandlrosedream.com) in organising their own
donations to help our people in Tonga. They donated over forty boxes of
baby nappies, office and school supplies and much more for the Nomuka
container.

* [Island Rose Dreams](http://www.islandrosedream.com). Island Rose
Dreams is a Tongan bath/body/beauty line made predominantly of Tongan
coconut oils infused with botanical herbs and plants. Check out their
amazing health and beauty range which can be ordered worldwide.

* [Outdoors Domain](https://www.outdoorsdomain.com.au) (28 Lionel Road,
Mount Waverley). Outdoors Domain sells a range of alfresco kitchens,
barbeques and outdoor furniture. They were kind enough to offer us a
great discount on their Gasmate cast iron ring burners. They have a
great range of stock, and their prices are second to none!

* [Mattress Builders](https://mattressbuilders.com.au/) (Warrigal Road,
Cheltenham).  Mattress Builders have a great range of mattresses, bases,
bed-frames and bedroom furniture. In addition they also carry Manchester
goods including sheets, quilt covers, quilts, pillows and mattress
protectors. Their friendly team were very supportive in getting the
orders done and delivered on time.

* My sincere gratitude to [Emmaus
College](https://www.emmaus.vic.edu.au/); Zoe Wood, Natalie Cosser and
Brigda DS. Thank you so much for all your support in organising the
collection of textbooks, library books and donating microscopes for
Tupouto'a College in the island of Nomuka. These are going to be
extremely useful resources for the college to use as they rebuild and
move forward. I cannot wait for the container to arrive and to get
updates on the college's new setup and library.

* I would also like to mention a special thank you to the wonderful
parents and families of both Mentone Girls Secondary College and
Parkdale Secondary College for all you have done to collect an
exceptionally large variety of textbooks from your schools. Your
contribution will be of great help for both teachers and students of
Tupouto'a College. Thank you again to Brigda DS for organising and
delivering that massive load of books.

* I would also like to acknowledge the wonderful Camberwell Grammar
School Pre-Loved Uniform Shop for donating their former school uniforms,
varying different sports uniforms, socks and footy boots for Tupouto'a
College to use. Thank you so much Kate Locke for organising all that.

To friends, families and all beautiful souls who jumped on board without
hesitation and made this relief project as success. I am lost for words,
and thank you seems inadequate. I am extremely grateful for all you have
done to help. Please know that your generosity and support will go a
long way; it will make a lot of lives easier to live, put smiles on
faces and give hope to those who struggle to carry on. I can only hope
and pray that your families and loved ones have all the best, good
health and more blessings in their life's journey. Some names to mention
(and I will add more names as I go):

* Caroline Rigby and her network of friends. Thank you for starting up
the Facebook post while I was still in a mess and not knowing where to
start. Thank you for all you have done to help and support, my friend.

* Michelle McIntyre, her sister Linda Burns, and their extended family
have been so generous throughout this relief project. Michelle, apart
from her multiple donations of various items, she donated four wooden
king single beds, organised and paid for brand new mattresses and sets
of bedding to go with them. Linda Burns donated four two burner stoves,
sets of pots and frying pans and many more needed items for the island.
Their parent,s children, grand-children and in-laws were on board.
Thank you so much for your family's generosity and support.

* Shirley and Michael Tarburton, thank you for organising donations from
your SDA congregation. Prof. Michael again for your biology books for
the school library in Nomuka. Tim and I really appreciate all you have
done to help.

* Yve Frankcombe, you are amazing! Thank you so much for all you have
done, your numerous donations, making sure the items collected were
washed before dropping off, and more. Your kindness and contribution
helped to make this cause a success.

* Carla Morris, your multiple donations will go a long way for a lot of
people. I used the gift card to buy two stoves from BBQ Galore. Thank
you so much!

* Jann and Tim Hurst and family.

* Lisa and colleagues.

* Claire B, Claire Bear, Julie MK, Babeth Perraud, Melissa Neow, Julina
Loh, Karen Beach, Elaine Chow, Edna Bonita, Gurmeet Singh, Sandra
Johnson, Amy Leembruggen, Lee Lee Anne, Taz Robertson, Julia, Angeli
Pickersgill, Cecilia Tse Tse, Deildint Megan Flemming, Laura Baxter,
Georgy Seg, Lee-Elise Chung, Avis Olivia, Angela Nham, Kerryn Jory,
Allie Starbuck, Maisy Buns, Julie Webster, Lisa Du, Beck Holder, Valerie
Lester, Amy Helen, Rochelle Starling, Bronwyn Bennett, Kandi Kokonet,
Elza James, Fione Wilson, Emma Le Gear-Uhe, Taz Robertson.

## Packing the container in Melbourne

The shipment was first scheduled to be packed on the 26th of March for
departure on the 30th--31st March. Then the ship was delayed until April
14.  We started packing on Saturday April 9. The wonderful Tongan
seasonal workers from Churchill were kind enough to come and help with
packing of the container. They did a fantastic job! Thank you Sitakio
Semisi (group leader) and your boys. Tim and I really appreciated your
help. On the second day of packing (11 April), everyone headed back to
work. Fortunately it was the beginning of the school holidays. I took my
girls to the container yard to continue packing. With the help of the
owners of Conan Cargo, Vai and Ramona Havili, packing was eventually
finished by the end of the day. The container was picked up from Conan
Cargo early on the morning of 12 April, and the ship left on April 14 to
Tonga! The container was meticulously packed, and there was space to fit
in a car for my office in the main island of Tongatapu. I had planned to send it
separately, but the boy's packing skills enabled it to fit in this
container. Everything els in the forty foot container goes to the
community of Nomuka.

Some photos of the container being packed are shown below. I wish I had
time to take more photos of everything that was packed.

We will update more news and photos when the container arrives in Tonga.

From the bottom of our hearts, thank you all!

Lupe and Tim Hume.

![nomuka](/images/nomuka/pack_001.jpg){:width="60%"} The seasonal workers helped pack the container.
![nomuka](/images/nomuka/pack_002.jpg){:width="60%"} Packing the container.
![nomuka](/images/nomuka/pack_003.jpg){:width="60%"} Drums of items for families in Nomuka.
![nomuka](/images/nomuka/pack_004.jpg){:width="60%"} Packing the container.
![nomuka](/images/nomuka/pack_005.jpg){:width="60%"} A pile of gas ring burners.
![nomuka](/images/nomuka/pack_006.jpg){:width="60%"} Gas ring burners.
![nomuka](/images/nomuka/pack_007.jpg){:width="60%"} The seasonal workers helped pack the container.
![nomuka](/images/nomuka/pack_008.jpg){:width="60%"} The container when empty.
![nomuka](/images/nomuka/pack_009.jpg){:width="60%"} Starting to fill the container.
![nomuka](/images/nomuka/pack_010.jpg){:width="60%"} Tiny beds for children.
![nomuka](/images/nomuka/pack_011.jpg){:width="60%"} What are they up to?
![nomuka](/images/nomuka/pack_012.jpg){:width="60%"} The items lined up ready for packing.
![nomuka](/images/nomuka/pack_013.jpg){:width="60%"} Drums of items for families.
![nomuka](/images/nomuka/pack_014.jpg){:width="60%"} Drums and packing boxes.
![nomuka](/images/nomuka/pack_015.jpg){:width="60%"} A lot of mattresses.
![nomuka](/images/nomuka/pack_016.jpg){:width="60%"} Drums and packing boxes.
![nomuka](/images/nomuka/pack_017.jpg){:width="60%"} Princess and the pea fairy tale?
![nomuka](/images/nomuka/pack_018.jpg){:width="60%"} Boxes by the dozen.
![nomuka](/images/nomuka/pack_019.jpg){:width="60%"} Another view of the items.
![nomuka](/images/nomuka/pack_020.jpg){:width="60%"} A lot of chairs.
![nomuka](/images/nomuka/pack_021.jpg){:width="60%"} This will keep the school stocked for decades.
![nomuka](/images/nomuka/pack_022.jpg){:width="60%"} School and sports uniforms.
![nomuka](/images/nomuka/pack_023.jpg){:width="60%"} Rugby is popular in Tonga.
![nomuka](/images/nomuka/pack_024.jpg){:width="60%"} The container is almost full.
![nomuka](/images/nomuka/pack_025.jpg){:width="60%"} Lots more chairs.
![nomuka](/images/nomuka/pack_026.jpg){:width="60%"} Two burner stoves.
![nomuka](/images/nomuka/pack_027.jpg){:width="60%"} Conan Cargo's forklift.
![nomuka](/images/nomuka/pack_028.jpg){:width="60%"} I guess the car needed protection.
![nomuka](/images/nomuka/pack_029.jpg){:width="60%"} Not much to go.
![nomuka](/images/nomuka/pack_030.jpg){:width="60%"} Stationary.
![nomuka](/images/nomuka/pack_031.jpg){:width="60%"} Paperwork is essential for cargo.
![nomuka](/images/nomuka/pack_032.jpg){:width="60%"} Two strong workers loading shelves.
![nomuka](/images/nomuka/pack_033.jpg){:width="60%"} Shelves.
![nomuka](/images/nomuka/pack_034.jpg){:width="60%"} Gardening tools and a walker.
![nomuka](/images/nomuka/pack_035.jpg){:width="60%"} Paperwork is visible for customs.
![nomuka](/images/nomuka/pack_036.jpg){:width="60%"} Keeping some buckets safe.

## In Nomuka

### GPS Loupata's library

![nomuka](/images/nomuka/nomuka_009.jpg) Unpacking the boxes.
![nomuka](/images/nomuka/nomuka_002.jpg) School uniforms.
![nomuka](/images/nomuka/nomuka_003.jpg) School uniforms.
![nomuka](/images/nomuka/nomuka_007.jpg) Distribution of school uniforms.
![nomuka](/images/nomuka/nomuka_004.jpg) Year 7 and 8 students.
![nomuka](/images/nomuka/nomuka_010.jpg) Year 7 and 8 students.
![nomuka](/images/nomuka/nomuka_020.jpg) Year 7 and 8 students.
![nomuka](/images/nomuka/nomuka_042.jpg) Year 7 and 8 students with their new jackets.
![nomuka](/images/nomuka/nomuka_027.jpg) Year 7 and 8 students with their new jackets.
![nomuka](/images/nomuka/nomuka_031.jpg) Year 7 and 8 students with their new jackets.
![nomuka](/images/nomuka/nomuka_045.jpg) Year 7 and 8 students with their new jackets.
![nomuka](/images/nomuka/nomuka_005.jpg) Arts and craft materials.
![nomuka](/images/nomuka/nomuka_006.jpg) Jigsaw puzzles.
![nomuka](/images/nomuka/nomuka_011.jpg) Toys.
![nomuka](/images/nomuka/nomuka_008.jpg) Miscellaneous items.
![nomuka](/images/nomuka/nomuka_001.jpg) A comfortable chair for reading.
![nomuka](/images/nomuka/nomuka_024.jpg) Welcome to the library.
![nomuka](/images/nomuka/nomuka_012.jpg) The library.
![nomuka](/images/nomuka/nomuka_015.jpg) The library.
![nomuka](/images/nomuka/nomuka_016.jpg) The library.
![nomuka](/images/nomuka/nomuka_013.jpg) Textbooks in the library.
![nomuka](/images/nomuka/nomuka_017.jpg) The teachers.
![nomuka](/images/nomuka/nomuka_014.jpg) Preparing shelf coverings.
![nomuka](/images/nomuka/nomuka_018.jpg) Preparing the shelf coverings.
![nomuka](/images/nomuka/nomuka_019.jpg) Shelves ready for books.
![nomuka](/images/nomuka/nomuka_021.jpg) Books and games for the younger students.
![nomuka](/images/nomuka/nomuka_022.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_023.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_030.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_034.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_025.jpg) Library rules.
![nomuka](/images/nomuka/nomuka_037.jpg) More library rules. The students love reading.
![nomuka](/images/nomuka/nomuka_026.jpg) Reading outside the library.
![nomuka](/images/nomuka/nomuka_029.jpg) Reading outside the library.
![nomuka](/images/nomuka/nomuka_028.jpg) Plenty of chairs.
![nomuka](/images/nomuka/nomuka_035.jpg) Showing off the books.
![nomuka](/images/nomuka/nomuka_032.jpg) Choosing books.
![nomuka](/images/nomuka/nomuka_033.jpg) Choosing books.
![nomuka](/images/nomuka/nomuka_047.jpg) Choosing a book.
![nomuka](/images/nomuka/nomuka_036.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_038.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_039.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_041.jpg) Reading time.
![nomuka](/images/nomuka/nomuka_043.jpg) The games are popular.
![nomuka](/images/nomuka/nomuka_040.jpg) I think she wants to play outside.
![nomuka](/images/nomuka/nomuka_044.jpg) A tidy library.
![nomuka](/images/nomuka/nomuka_046.jpg) After school.

[^fn-alaska]: Maggie Nelson, [*The eruption near Tonga was so powerful you could hear it in Alaska*](https://alaskapublic.org/2022/01/17/the-eruption-near-tonga-was-so-powerful-you-could-hear-it-in-alaska/), (Unalaska: Alaska Public Media, January 17 2022).
